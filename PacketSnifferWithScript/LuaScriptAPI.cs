﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using System.Windows.Forms;
using System.IO;
namespace PacketSnifferWithScript
{
    public class LuaScriptAPI : Lua
    {
        private ListBox ScriptBox { get; set; }
        private HexBox HexBox { get; set; }
        public Dictionary<string, int> TypeList = new Dictionary<string, int>();//key Name Value Lenght
        private BinaryReader reader { get; set; }

        public LuaScriptAPI(ListBox pBox,HexBox HBox,byte[] buffer)
        {
            ScriptBox = pBox;
            HexBox = HBox;
            MemoryStream MemStream = new MemoryStream(buffer);
            reader = new BinaryReader(MemStream);
            RegisterFunction("AddByte", this, this.GetType().GetMethod("AddByte"));
            RegisterFunction("AddSByte", this, this.GetType().GetMethod("AddSByte"));
            RegisterFunction("AddShort", this, this.GetType().GetMethod("AddShort"));
            RegisterFunction("AddUShort", this, this.GetType().GetMethod("AddUShort"));
            RegisterFunction("AddInt", this, this.GetType().GetMethod("AddInt"));
            RegisterFunction("AddUInt", this, this.GetType().GetMethod("AddUInt"));
            RegisterFunction("AddLong", this, this.GetType().GetMethod("AddLong"));
            RegisterFunction("AddULong", this, this.GetType().GetMethod("AddULong"));
            RegisterFunction("print", this, this.GetType().GetMethod("print"));
            RegisterFunction("AddNote", this, this.GetType().GetMethod("AddNote"));

        }
        public void SetStreamPos(long pos)
        {
            reader.BaseStream.Position = pos;
        }
        public void print(string Message)
        {
            MessageBox.Show(Message);
        }
        public void AddNote(string Note)
        {
            ScriptBox.Items.Add(Note);
        }
        public byte AddByte(string Name)
        {
            ScriptBox.Items.Add(Name);
            byte value = reader.ReadByte();
            if(!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 1);
            return value;
        }
        public int CalcStart(string index)
        {
            lock (index)
            {
                int Start = 0;
                var ScreachItem = TypeList[index];
                foreach (var Item in TypeList)
                {
                    if (Item.Key == ScreachItem.ToString())
                    {
                        break;
                    }
                    Start += Item.Value;
                }

                return Start;
            }
        }
        public sbyte AddSByte(string Name)
        {
            ScriptBox.Items.Add(Name);

            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 1);
            return reader.ReadSByte();
        }
        public short AddShort(string Name)
        {
            ScriptBox.Items.Add(Name);
            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 2);
            return reader.ReadInt16();
        }
        public UInt16 AddUShort(string Name)
        {
            ScriptBox.Items.Add(Name);
            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 2);
            return reader.ReadUInt16();
        }
        public int AddInt(string Name)
        {
            ScriptBox.Items.Add(Name);
            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 4);
            return reader.ReadInt32();
        }
        public uint AddUInt(string Name)
        {
            ScriptBox.Items.Add(Name);
            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 4);
            return reader.ReadUInt32();
        }
        public long AddLong(string Name)
        {
            ScriptBox.Items.Add(Name);
            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 8);
            return reader.ReadInt64();
        }
        public ulong AddULong(string Name)
        {
            ScriptBox.Items.Add(Name);
            if (!TypeList.ContainsKey(Name))
            TypeList.Add(Name, 8);
            return reader.ReadUInt64();
        }
    }
}
