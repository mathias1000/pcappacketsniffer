﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace PacketSnifferWithScript
{
    public class Config
    {
        public int MinPort { get; set; }
        public int MaxPort { get; set; }
        public string pcapFilter { get; set; }
        public static Config Instance { get; set; }

        public void WriteXML()
        {
         
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Config));

            System.IO.StreamWriter file = new System.IO.StreamWriter(
                @"Config.xml");
            writer.Serialize(file, this);
            file.Close();
        }
        public void ReadXML()
        {
            if(File.Exists(@"Config.xml"))
            {
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Config));
            System.IO.StreamReader file = new System.IO.StreamReader(@"Config.xml");
           Config cfg = (Config)reader.Deserialize(file);
           Config.Instance = cfg;
           file.Close();
            }
        }

    }
}
