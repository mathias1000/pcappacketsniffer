﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace PacketSnifferWithScript
{
    public partial class ScriptControl : Form
    {
        public MainForm Main { get; set; }
        public LuaScriptAPI Lua { get; set; }
        public byte[] buffer { get; set; }
        public ScriptControl(MainForm main,byte[] buffer)
        {
          //  main.hexBox1.
            InitializeComponent();
            Main = main;
            hexBox1.ByteProvider = new DynamicByteProvider(buffer);
            Lua = new LuaScriptAPI(ScriptViewBox,main.hexBox1,buffer);
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
           // openFileDialog.Filter = "lua files (*.lua)|*.lua";
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    string text = File.ReadAllText(file);
                    Lua.DoString(text);
                    syntaxRichTextBox1.Text = text;
                 
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        private void hextbox1_SelectionLengthChanged(object pSender, EventArgs pArgs)
        {
            HexBox hex = pSender as HexBox;
            byte[] data = hex.ByteProvider.ReadAllByte();

            propertyGrid1.SelectedObject = new StructureSegment(data, (int)hex.SelectionStart, (int)hex.SelectionLength);
        }
        private void ScriptViewBox_SelectedValueChanged(object sender, EventArgs e)
        {
            lock(sender)
            {
            ListBox Listbox = sender as ListBox;
            if (Listbox.SelectedItem != null)
            {
                int start = Lua.CalcStart(Listbox.SelectedItem.ToString());
                int Lenght;
                if (!Lua.TypeList.TryGetValue(Listbox.SelectedItem.ToString(), out Lenght))
                {
                    return;
                }
                Main.hexBox1.Select(start, Lenght);
                this.hexBox1.Select(start, Lenght);
            }
            }
          //  int start = Lua.CalcStart(control.);
        }

        private void AddByteButton_Click(object sender, EventArgs e)
        {
           
        }

        private void syntaxRichTextBox1_RightToLeftChanged(object sender, EventArgs e)
        {

        }

        private void SaveB_Click(object sender, EventArgs e)
        {
            try
            {
             DialogResult result = saveFileDialog1.ShowDialog(); // Show the dialog.
           // openFileDialog.Filter = "lua files (*.lua)|*.lua";
             if (result == DialogResult.OK) // Test result.
             {
                 string file = openFileDialog1.FileName;
                 File.WriteAllText(file, syntaxRichTextBox1.Text);
                 ScriptViewBox.Items.Clear();
                 Lua.SetStreamPos(0);//set binary pos 0 for read again
                 Lua.DoFile(file);
             }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ReloadB_Click(object sender, EventArgs e)
        {
            try
            {
                File.WriteAllText(openFileDialog1.FileName, syntaxRichTextBox1.Text);
                ScriptViewBox.Items.Clear();
                Lua.SetStreamPos(0);
                Lua.DoFile(openFileDialog1.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
      
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = openFileDialog2.ShowDialog(); // Show the dialog.
                if (result == DialogResult.OK) // Test result.
                {
                    string file = openFileDialog2.FileName;
                    byte[] FileText = File.ReadAllBytes(file);
                    Lua = new LuaScriptAPI(ScriptViewBox, hexBox1, FileText);
                    ScriptViewBox.Items.Clear();
                    hexBox1.ByteProvider = new DynamicByteProvider(FileText);
                    MessageBox.Show("Load File " + openFileDialog2.FileName + " OK");
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = saveFileDialog2.ShowDialog(); // Show the dialog.
                if (result == DialogResult.OK) // Test result.
                {
                    string file = saveFileDialog2.FileName;
                    ScriptViewBox.Items.Clear();
                    Lua = null;
                   syntaxRichTextBox1.Text = ""; //clear when save?
                    File.WriteAllBytes(file, hexBox1.ByteProvider.ReadAllByte());

                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ScriptControl_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
        
    }
}
