﻿namespace PacketSnifferWithScript
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startSniffingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopSniffingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scriptControllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PacketViewList = new System.Windows.Forms.DataGridView();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.hexBox1 = new System.Windows.Forms.HexBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PacketViewList)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(648, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startSniffingToolStripMenuItem,
            this.stopSniffingToolStripMenuItem,
            this.configToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // startSniffingToolStripMenuItem
            // 
            this.startSniffingToolStripMenuItem.Name = "startSniffingToolStripMenuItem";
            this.startSniffingToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.startSniffingToolStripMenuItem.Text = "Start Sniffing";
            this.startSniffingToolStripMenuItem.Click += new System.EventHandler(this.startSniffingToolStripMenuItem_Click);
            // 
            // stopSniffingToolStripMenuItem
            // 
            this.stopSniffingToolStripMenuItem.Name = "stopSniffingToolStripMenuItem";
            this.stopSniffingToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.stopSniffingToolStripMenuItem.Text = "Stop Sniffing";
            this.stopSniffingToolStripMenuItem.Click += new System.EventHandler(this.stopSniffingToolStripMenuItem_Click);
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.configToolStripMenuItem.Text = "Config";
            this.configToolStripMenuItem.Click += new System.EventHandler(this.configToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scriptControllToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // scriptControllToolStripMenuItem
            // 
            this.scriptControllToolStripMenuItem.Name = "scriptControllToolStripMenuItem";
            this.scriptControllToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.scriptControllToolStripMenuItem.Text = "ScriptControll";
            this.scriptControllToolStripMenuItem.Click += new System.EventHandler(this.scriptControllToolStripMenuItem_Click);
            // 
            // PacketViewList
            // 
            this.PacketViewList.AllowUserToAddRows = false;
            this.PacketViewList.AllowUserToDeleteRows = false;
            this.PacketViewList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PacketViewList.Location = new System.Drawing.Point(0, 24);
            this.PacketViewList.MultiSelect = false;
            this.PacketViewList.Name = "PacketViewList";
            this.PacketViewList.ReadOnly = true;
            this.PacketViewList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PacketViewList.Size = new System.Drawing.Size(649, 183);
            this.PacketViewList.TabIndex = 1;
            this.PacketViewList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PacketViewList_CellContentClick);
            this.PacketViewList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PacketViewList_CellContentDoubleClick);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.CommandsVisibleIfAvailable = false;
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 409);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid1.Size = new System.Drawing.Size(286, 147);
            this.propertyGrid1.TabIndex = 4;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // hexBox1
            // 
            this.hexBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Table;
            this.hexBox1.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hexBox1.LineInfoForeColor = System.Drawing.Color.Empty;
            this.hexBox1.LineInfoVisible = true;
            this.hexBox1.Location = new System.Drawing.Point(0, 207);
            this.hexBox1.Name = "hexBox1";
            this.hexBox1.ReadOnly = true;
            this.hexBox1.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this.hexBox1.Size = new System.Drawing.Size(649, 200);
            this.hexBox1.StringViewVisible = true;
            this.hexBox1.TabIndex = 0;
            this.hexBox1.UseFixedBytesPerLine = true;
            this.hexBox1.VScrollBarVisible = true;
            this.hexBox1.SelectionLengthChanged += new System.EventHandler(this.hextbox1_SelectionLengthChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 557);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.PacketViewList);
            this.Controls.Add(this.hexBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "PacketSniffer By Mathias1000";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Enter += new System.EventHandler(this.MainForm_Enter);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PacketViewList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startSniffingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopSniffingToolStripMenuItem;
        private System.Windows.Forms.DataGridView PacketViewList;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        public System.Windows.Forms.HexBox hexBox1;
        private System.Windows.Forms.ToolStripMenuItem scriptControllToolStripMenuItem;
    }
}