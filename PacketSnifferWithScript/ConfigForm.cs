﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using PacketDotNet.LLDP;
using SharpPcap;
using System.Threading;

namespace PacketSnifferWithScript
{
    public partial class ConfigForm : Form
    {
        private ICaptureDevice dev { get; set; }
        public ConfigForm(ICaptureDevice Device)
        {
            InitializeComponent();
            dev = Device;
            textBox1.Text = Device.Filter;
        }

        private void abrechen_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
                Config.Instance.ReadXML();
                textBox1.Text = Config.Instance.pcapFilter;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            lock (sender)
            {
                try
                {
     
                    dev.Filter = textBox1.Text;
                    Config.Instance.pcapFilter = dev.Filter;
                    Config.Instance.WriteXML();
                    MessageBox.Show("Saving Sucess");
                }
                catch (PcapException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Pcap SetFilter Eror");
                }
            }
        }
    }
}
