﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using PacketDotNet.LLDP;
using SharpPcap;

namespace PacketSnifferWithScript
{
    public partial class InterfaceForm : Form
    {
        public InterfaceForm()
        {
            InitializeComponent();
            label2.Text = SharpPcap.Version.VersionString;
            /* Retrieve the device list */
            var devices = CaptureDeviceList.Instance;
             Config.Instance = new Config();
            /*If no device exists, print error */
            if (devices.Count < 1)
            {
                MessageBox.Show("No device found on this machine");
                Application.Exit();
            }
            /* Scan the list and to comobobox */
            foreach (var dev in devices)
            {
                comboBox1.Items.Add(dev.Description);
            }
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void comboBox1_TextUpdate(object sender, EventArgs e)
        {
            comboBox1.Text = "";
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string curItem = comboBox1.SelectedItem.ToString();
            int index = comboBox1.FindString(curItem);
            if (index == -1)
                MessageBox.Show("Item is not available in SelectBox");
            else
            {
                try
                {
                    button1.Enabled = true;
                    
                }
                catch
                {
                    MessageBox.Show("Invalid Service Selectet");
                    Application.Exit();

                }
            }
         }

        private void button1_Click(object sender, EventArgs e)
        {
            string curItem = comboBox1.SelectedItem.ToString();
            int index = comboBox1.FindString(curItem);
            var device = CaptureDeviceList.Instance[index];
            int readTimeoutMilliseconds = 1000;
            device.Open(DeviceMode.Promiscuous, readTimeoutMilliseconds);
            Config.Instance.MaxPort = (int)maxport.Value;
            Config.Instance.MinPort = (int)MinPort.Value;        
            device.Filter = string.Format("tcp portrange {0}-{1}", MinPort.Value, maxport.Value);//todo save config
            Config.Instance.pcapFilter = device.Filter;
           Form MainForm = new MainForm(device);
            
           MainForm.Show();
           Program.InterfaceForm.Close();

        }
        private void MinPort_ValueChanged(object sender, EventArgs e)
        {
            if (MinPort.Value > maxport.Value) MinPort.Value = maxport.Value;
        }
        private void mHighPortNumeric_ValueChanged(object pSender, EventArgs pArgs)
        {
            if (maxport.Value < MinPort.Value) maxport.Value = maxport.Value;
        }
    }
}
