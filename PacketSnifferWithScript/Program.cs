﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PacketSnifferWithScript
{
    static class Program
    {
        public static Form InterfaceForm { get; set; }

        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InterfaceForm = new InterfaceForm();
            InterfaceForm.Show();
            Application.Run();
        }
    }
}
