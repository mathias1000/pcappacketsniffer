﻿using System.Drawing;

namespace PacketSnifferWithScript
{
    partial class ScriptControl
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScriptViewBox = new System.Windows.Forms.ListBox();
            this.syntaxRichTextBox1 = new SyntaxHighlighter.SyntaxRichTextBox();
            this.SaveB = new System.Windows.Forms.Button();
            this.ReloadB = new System.Windows.Forms.Button();
            this.OpenButton = new System.Windows.Forms.Button();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.hexBox1 = new System.Windows.Forms.HexBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // ScriptViewBox
            // 
            this.ScriptViewBox.FormattingEnabled = true;
            this.ScriptViewBox.Location = new System.Drawing.Point(482, 22);
            this.ScriptViewBox.Name = "ScriptViewBox";
            this.ScriptViewBox.Size = new System.Drawing.Size(205, 225);
            this.ScriptViewBox.TabIndex = 48;
            this.ScriptViewBox.SelectedValueChanged += new System.EventHandler(this.ScriptViewBox_SelectedValueChanged);
            // 
            // syntaxRichTextBox1
            // 
            this.syntaxRichTextBox1.AcceptsTab = true;
            this.syntaxRichTextBox1.AutoWordSelection = true;
            this.syntaxRichTextBox1.Location = new System.Drawing.Point(2, 258);
            this.syntaxRichTextBox1.Name = "syntaxRichTextBox1";
            this.syntaxRichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal;
            this.syntaxRichTextBox1.ShowSelectionMargin = true;
            this.syntaxRichTextBox1.Size = new System.Drawing.Size(685, 367);
            this.syntaxRichTextBox1.TabIndex = 49;
            this.syntaxRichTextBox1.Text = "";
            this.syntaxRichTextBox1.RightToLeftChanged += new System.EventHandler(this.syntaxRichTextBox1_RightToLeftChanged);
            // 
            // SaveB
            // 
            this.SaveB.Location = new System.Drawing.Point(605, 653);
            this.SaveB.Name = "SaveB";
            this.SaveB.Size = new System.Drawing.Size(75, 23);
            this.SaveB.TabIndex = 50;
            this.SaveB.Text = "Save";
            this.SaveB.UseVisualStyleBackColor = true;
            this.SaveB.Click += new System.EventHandler(this.SaveB_Click);
            // 
            // ReloadB
            // 
            this.ReloadB.Location = new System.Drawing.Point(605, 682);
            this.ReloadB.Name = "ReloadB";
            this.ReloadB.Size = new System.Drawing.Size(75, 23);
            this.ReloadB.TabIndex = 51;
            this.ReloadB.Text = "Reload";
            this.ReloadB.UseVisualStyleBackColor = true;
            this.ReloadB.Click += new System.EventHandler(this.ReloadB_Click);
            // 
            // OpenButton
            // 
            this.OpenButton.Location = new System.Drawing.Point(605, 711);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(75, 23);
            this.OpenButton.TabIndex = 52;
            this.OpenButton.Text = "Open";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.CommandsVisibleIfAvailable = false;
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.Location = new System.Drawing.Point(2, 9);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid1.Size = new System.Drawing.Size(474, 147);
            this.propertyGrid1.TabIndex = 54;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.Filter = "*.lua*";
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "lua files (*.lua)|*.lua";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 206);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 55;
            this.button1.Text = "Load Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(102, 206);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 56;
            this.button2.Text = "SaveData";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "lua files (*.lua)|*.lua";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            this.openFileDialog2.Filter = "dat files (*.dat)|*.dat";
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.Filter = "dat files (*.dat)|*.dat";
            // 
            // hexBox1
            // 
            this.hexBox1.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hexBox1.LineInfoForeColor = System.Drawing.Color.Empty;
            this.hexBox1.Location = new System.Drawing.Point(2, 631);
            this.hexBox1.Name = "hexBox1";
            this.hexBox1.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this.hexBox1.Size = new System.Drawing.Size(597, 113);
            this.hexBox1.TabIndex = 53;
            this.hexBox1.SelectionLengthChanged += new System.EventHandler(this.hextbox1_SelectionLengthChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 25);
            this.label1.TabIndex = 57;
            this.label1.Text = "Binary Controll";
            // 
            // ScriptControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 746);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.hexBox1);
            this.Controls.Add(this.OpenButton);
            this.Controls.Add(this.ReloadB);
            this.Controls.Add(this.SaveB);
            this.Controls.Add(this.syntaxRichTextBox1);
            this.Controls.Add(this.ScriptViewBox);
            this.Name = "ScriptControl";
            this.Text = "ScriptControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScriptControl_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ScriptViewBox;
        private SyntaxHighlighter.SyntaxRichTextBox syntaxRichTextBox1;
        private System.Windows.Forms.Button SaveB;
        private System.Windows.Forms.Button ReloadB;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.HexBox hexBox1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.Label label1;
    }
}