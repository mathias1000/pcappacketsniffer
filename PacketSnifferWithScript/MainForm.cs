﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using PacketDotNet.LLDP;
using SharpPcap;
using System.Threading;
using System.Xml;

namespace PacketSnifferWithScript
{

    public delegate void PacketEventHandler(object e ,bool type);

    public partial class MainForm : Form
    {
        public byte[] targetPacket { get; set; }
        private ICaptureDevice MainDevice { get; set; }
        private Thread DeviceThread { get; set; }
        private string DeviceIp { get; set; }

        public MainForm(ICaptureDevice Device)
        {
            InitializeComponent();
            MainDevice = Device;
            DeviceIp = GetDeviceIP();
            InitPacketView();
        }
        private string GetDeviceIP()
        {

            var devices = CaptureDeviceList.Instance;
            foreach (WinPcapDevice dev in devices)
            {
                Console.Out.WriteLine("{0}", dev.Description);

                foreach (PcapAddress addr in dev.Addresses)
                {
                    if (addr.Addr != null && addr.Addr.ipAddress != null)
                    {
                        return addr.Addr.ipAddress.ToString();
                    }
                }
            }
            return null;
        }
        private void device_OnPacketArrival(object sender, CaptureEventArgs e)
        {
            try
            {
                var packet = PacketDotNet.Packet.ParsePacket(e.Packet);
                var tcpPacket = PacketDotNet.TcpPacket.GetEncapsulated(packet);
                var UDPPacket = PacketDotNet.UdpPacket.GetEncapsulated(packet);
                string data = String.Format("{0:X}", BitConverter.ToString(packet.Bytes).Replace("-", " "));
                if (tcpPacket != null)
                {
                    var ipPacket = (PacketDotNet.IpPacket)tcpPacket.ParentPacket;
                    if (ipPacket.SourceAddress.ToString() == DeviceIp)
                    {
                        //   PacketViewList.Rows.Add(tcpPacket.SourcePort, "TCP", ipPacket.SourceAddress, ipPacket.DestinationAddress, BitConverter.ToString(packet.Bytes).Replace("-", string.Empty));
                        MethodInvoker Packet = delegate
                        {
                            
                            PacketViewList.Rows.Add(tcpPacket.DestinationPort, "TCP", ipPacket.SourceAddress, ipPacket.DestinationAddress, data, packet.Bytes.Length, DateTime.Now);
                        };
                        this.Invoke(Packet);

                    }
                    else
                    {
                        MethodInvoker Packet = delegate
                        {
                            PacketViewList.Rows.Add(tcpPacket.DestinationPort, "TCP", ipPacket.SourceAddress, ipPacket.DestinationAddress, data, packet.Bytes.Length, DateTime.Now);
                        };
                        this.Invoke(Packet);

                    }
                }
                else if (UDPPacket != null)
                {

                    var ipPacket = (PacketDotNet.IpPacket)UDPPacket.ParentPacket;
                    if (ipPacket.SourceAddress.ToString() == DeviceIp)
                    {
                        MethodInvoker Packet = delegate
                        {
                            PacketViewList.Rows.Add(UDPPacket.DestinationPort, "UDP", ipPacket.SourceAddress, ipPacket.DestinationAddress, data,packet.Bytes.Length,DateTime.Now);
                        };
                        this.Invoke(Packet);
                    }
                    else
                    {
                        MethodInvoker Packet = delegate
                           {
                               PacketViewList.Rows.Add(UDPPacket.DestinationPort, "UDP", ipPacket.SourceAddress, ipPacket.DestinationAddress, data, packet.Bytes.Length, DateTime.Now);
                           };
                        this.Invoke(Packet);
                    }
                }
            }
            catch
            {
                //catch rows when down?
            }
        }
        private void InitPacketView()
        {
            PacketViewList.ColumnCount = 7;
            PacketViewList.Columns[0].Name = "Port";
            PacketViewList.Columns[1].Name = "Protokoll";
            PacketViewList.Columns[2].Name = "source Adress";
            PacketViewList.Columns[3].Name = "destination Adress";
            PacketViewList.Columns[4].Name = "Data";
            PacketViewList.Columns[5].Name = "DataLenght";
            PacketViewList.Columns[6].Name = "Packet Time";
            //PacketViewList.Columns[7].Name = "PacketName";
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            MainDevice.Filter = Config.Instance.pcapFilter;
            //Register our handler function to the 'packet arrival' event
            MainDevice.OnPacketArrival += new PacketArrivalEventHandler(device_OnPacketArrival);//register packet event
            Thread SnifferThread = new Thread(new ThreadStart(MainDevice.Capture));  //MainDevice.Capture(); // Start capture 'INFINTE' number of packets
            // MainDevice.Capture();
            SnifferThread.Start();
            SetSniffingOnline();
        }

        private void startSniffingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DeviceThread == null)
            {
                Thread SnifferThread = new Thread(new ThreadStart(MainDevice.Capture));  //MainDevice.Capture(); // Start capture 'INFINTE' number of packets
                SnifferThread.Start();
                SetSniffingOnline();
            }
            else
            {
                MessageBox.Show("Sniffing Alredy Startet");
            }
        }
        private void SetSniffingOnline()
        {
            //todo set online Pictures
        }
        private void SetSniffingOffline()
        {
            //todo set offline Pictures
        }

        private void stopSniffingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DeviceThread != null)
            {
                DeviceThread.Abort();
                SetSniffingOffline();
            }
        }

        private void PacketViewList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)//check if datacolumn
            {
                DataGridViewRow row = PacketViewList.Rows[e.RowIndex];
                string[] val = row.AccessibilityObject.Value.Split(';');
                string data = val[e.ColumnIndex].Replace(" ", "");
                byte[] buffer = data.HexToBytes();
                hexBox1.ByteProvider = new DynamicByteProvider(buffer);
                targetPacket = buffer;

            }
        }

        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form CfgForm = new ConfigForm(MainDevice);
            CfgForm.Show();
        }

        private void MainForm_Enter(object sender, EventArgs e)
        {

        }


        private void hextbox1_SelectionLengthChanged(object pSender, EventArgs pArgs)
        {
            HexBox hex = pSender as HexBox;
            byte[] data = hex.ByteProvider.ReadAllByte();

            propertyGrid1.SelectedObject = new StructureSegment(data, (int)hex.SelectionStart, (int)hex.SelectionLength);
        }
        private void hexBox1_Click(object sender, EventArgs e)
        {
        
        }
        private void PacketViewList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (hexBox1.ByteProvider == null)
            {
                MessageBox.Show("You Have Not Packet in Target");
                return;
            }

            byte[] hexboxdata = hexBox1.ByteProvider.ReadAllByte();
            if (hexboxdata != targetPacket)
            {
                Form scriptc = new ScriptControl(this, hexboxdata);
                scriptc.Show();
                return;
            }

            Form script = new ScriptControl(this, hexboxdata);
            script.Show();
        }

        private void scriptControllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScriptControl scriptform = new ScriptControl(this,new byte[]{});
            scriptform.ShowDialog();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (System.IO.File.Exists(@"Config.xml"))
            {
                System.IO.File.Delete(@"Config.xml");
            }
            Config.Instance.WriteXML();
        }
    }
}
